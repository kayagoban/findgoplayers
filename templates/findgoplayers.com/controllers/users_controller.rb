module Templates
  module FindGoPlayers
    module UsersController
      extend ActiveSupport::Concern

      def block_info_message
        "FindGoPlayers will no longer forward messages from #{@user.nickname}"
      end

      def visible
        Rails.cache.fetch("visible_users", expires_in: 2.minutes ) do
          User.visible
        end
      end

      def send_user_message
        GoMailer.user_message(@user, current_user, @message).deliver
      end

      def display_name
        if @user.player?
          @user.name_and_rank
        else
          @user.nickname
        end
      end

    end 


  end
end

