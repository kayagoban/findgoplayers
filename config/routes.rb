Rails.application.routes.draw do

  get 'feed' => 'conversations#feed'

  resources :conversations, only: [:create, :index] do
    get 'comments'
    post 'comment'
    get 'like_toggle'
    collection do
      get 'home'
      get 'top'
    end
  end

  root :to => 'users#index'

  devise_for :users, :controllers => { :confirmations => 'confirmations', registrations: 'registrations', omniauth_callbacks: 'omniauth_callbacks', sessions: 'sessions' }

  devise_scope :user do
    get 'account' => 'registrations#edit'
    get "signout", to: "devise/sessions#destroy"
  end

  # Users
  resources :users, only: [:index, :show] do
    get 'conversations' => 'conversations#user_feed'
    member do
      get 'tester'
      get 'new_message'
      post 'post_message'
      get  'block'
      post 'post_location_update'
    end
    collection do
      get 'update_location'
    end
  end

  #get 'about_us' => 'static#about_us'
  get 'how_it_works' => 'static#how_it_works'
  get 'hire_me' => 'static#hire_me'
  get 'contact' => 'static#contact'
  
  # Omniauth
  get 'omniauth_callbacks/facebook'
  get 'omniauth_callbacks/linkedin'
  get 'omniauth_callbacks/google'


  #constraints(:host => "otherdomain.com") do
  #  devise_for :users, :controllers => { registrations: 'trations' }
  #end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
