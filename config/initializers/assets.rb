
Rails.application.config.assets.precompile += %w( map.js )
Rails.application.config.assets.precompile += %w( edit_user_location.js )
Rails.application.config.assets.precompile += %w( markerclusterer_compiled.js )
Rails.application.config.assets.precompile += %w( mapiconmaker.js )
Rails.application.config.assets.precompile += %w( registrations_edit.js )
Rails.application.config.assets.precompile += %w( registrations_has_latlng.js )
Rails.application.config.assets.precompile += %w( registrations_no_latlng.js )
Rails.application.config.assets.precompile += %w( conversations.css )
Rails.application.config.assets.precompile += %w( animate.min.css )
Rails.application.config.assets.precompile += %w( update_locations.js )

Rails.application.config.assets.precompile += %w( findgoplayers.com/map.js )
