# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Some users
u = User.create!(email: "me@me.com", nickname: "Samuel L. Jackson", rank: -5, lat: 32.1, lng: 32.1, password: 'qweruiop', password_confirmation: 'qweruiop', confirmed_at: Time.now)
u2 = User.create!(email: "other@me.com", nickname: "Hikaru3928", rank: 6, lat: 32.1, lng: 32.1, password: 'qweruiop', password_confirmation: 'qweruiop', confirmed_at: Time.now)
u3 = User.create!(email: "brad@me.com", nickname: "Brad", rank: 27, lat: 32.1, lng: 32.1, password: 'qweruiop', password_confirmation: 'qweruiop', confirmed_at: Time.now)

# Conversation

conv = Conversation.create!
Comment.create!(user: u, text: "Normally, both your asses would be dead as fucking fried chicken, but you happen to pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna help you. But I can't give you this case, it don't belong to me. Besides, I've already been through too much shit this morning over this case to hand it over to your dumb ass.",
 conversation: conv)
Comment.create!(user: u2, text: "Hey, good post!", conversation: conv)
Comment.create!(user: u, text: "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", conversation: conv)
Comment.create!(user: u2, text: "What?", conversation: conv)
Comment.create!(user: u, text: "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", conversation: conv)
Like.create(user: u, conversation: conv)


conv2 = Conversation.create!
Comment.create!(user: u, text: "Know wut I herd?", conversation: conv2)
Comment.create!(user: u2, text: "Oh no you didn't", conversation: conv2)
Comment.create!(user: u3, text: "Oh no you didn't", conversation: conv2)

conv3 = Conversation.create!
Comment.create!(user: u2, text: "Let's make a go club", conversation: conv3)

conv4 = Conversation.create!
Comment.create!(user: u, text: "BLah blah blha blah", conversation: conv4)
Comment.create!(user: u2, text: "You are wrong because X", conversation: conv4)
Like.create(user: u, conversation: conv4)
Like.create(user: u2, conversation: conv4)
Like.create(user: u3, conversation: conv4)


