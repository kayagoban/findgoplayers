class AddCountsToConversation < ActiveRecord::Migration
  def change
    add_column :conversations, :likes_count, :integer, default: 0
    add_column :conversations, :comments_count, :integer, default: 0
  end
end
