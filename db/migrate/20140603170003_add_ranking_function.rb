class AddRankingFunction < ActiveRecord::Migration
  def change
    execute <<-SQLTEXT

    CREATE FUNCTION popularity(count integer, weight integer default 3) RETURNS integer AS $$
      SELECT count * weight
    $$ LANGUAGE SQL IMMUTABLE;

    CREATE FUNCTION ranking(id integer, comments integer, likes integer, popularity_weight integer) RETURNS integer AS $$
      SELECT id + ( ( comments + likes) * popularity_weight )
    $$ LANGUAGE SQL IMMUTABLE;

    CREATE INDEX index_comments_on_ranking
      ON conversations (ranking(id, comments_count, likes_count, 3) DESC);

    SQLTEXT
  end
end
