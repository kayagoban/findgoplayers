class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :lat, :float
    add_column :users, :lng, :float
    add_column :users, :name, :string
    add_column :users, :rank, :integer, default: 30
    add_column :users, :blurb, :text
    add_column :users, :url, :string
    add_column :users, :user_type, :integer, default: 0
    add_column :users, :avatar, :string
    add_column :users, :is_go_player, :boolean, default: false
  end
end
