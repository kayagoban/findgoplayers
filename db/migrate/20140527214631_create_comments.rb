class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :conversation, index: true
      t.references :user, index: true
      t.text :text
      t.string :url
      t.string :url_title
      t.string :url_description
      t.string :url_thumbnail
      t.string :url_video

      t.timestamps
    end
  end
end
