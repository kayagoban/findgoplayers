class AddMsgCountersToUser < ActiveRecord::Migration
  def change
    add_column :users, :last_message_at, :datetime, default: Time.now
    add_column :users, :msg_count, :integer, default: 0
  end
end
