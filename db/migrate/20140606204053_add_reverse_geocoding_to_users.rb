class AddReverseGeocodingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :loc_string, :string
    add_column :users, :locality, :string
    add_column :users, :admin_area_1, :string
    add_column :users, :country, :string
  end
end
