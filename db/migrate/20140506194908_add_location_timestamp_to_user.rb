class AddLocationTimestampToUser < ActiveRecord::Migration
  def change
    add_column :users, :location_timestamp, :datetime
    add_column :users, :notification_distance, :float, default: 8
  end
end
