class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.references :conversation
      t.references :user

      t.timestamps
    end
    add_index :likes, [:user_id, :conversation_id]
  end
end
