class AddDisableNotificationsToUser < ActiveRecord::Migration
  def change
    add_column :users, :disable_notifications, :boolean, default: false
  end
end
