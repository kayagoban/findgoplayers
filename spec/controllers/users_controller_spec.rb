require 'spec_helper'

describe UsersController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'message'" do
    it "returns http success" do
      get 'message'
      response.should be_success
    end
  end

end
