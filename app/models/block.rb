class Block < ActiveRecord::Base
  belongs_to :user
  belongs_to :blockee, class_name: 'User'
end
