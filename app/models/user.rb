class User < ActiveRecord::Base

  has_many :blocks
  has_many :blockees, through: :blocks

  has_many :conversations, through: :comments
  has_many :comments, dependent: :destroy

  # For omniauthable
  has_many :identities

  # Include default devise modules. Others available are:
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :confirmable, :lockable

  acts_as_mappable #:default_formula => :sphere, #:distance_field_name => :distance,

  #validate on update only
  validates_presence_of :nickname, :on => [ :update ]
  validate :map_position_validation, :on => [ :update ]

  # Pro rank requires heavenly dispensation
  validates :rank, :numericality => { :greater_than => -8 } 

  validates :notification_distance, :numericality => { greater_than: 0, less_than: 101 }

  #TODO (kayagoban) put this in a method that scopes on domain
  scope :visible, -> { User.where.not(lat: nil, lng: nil) }

  scope :listenable_for, 
    ->(listener) { within(listener.notification_distance, origin: listener) }

  #NOTE (kayagoban) This pull operation is controlled by the users's notification distance
  #def listenable_users
  #  self.class.within(notification_distance, origin: self) - blockees
  #end


  include GoPlayer

  MAX_MESSAGES_PER_DAY = 20

  TEMP_EMAIL = 'change@me.com'
  TEMP_EMAIL_REGEX = /change@me.com/

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update

  #NOTE (kayagoban) Used by mailer to wean down possible recipients.
  #                 Since it's a push operation, we regulate the potential area
  def nearby_users(default_distance)
    self.class.within(default_distance, origin: self) - [ self ]
  end


  def self.find_for_oauth(auth, signed_in_resource = nil)
    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)
    user = identity.user
    return user unless user.nil?

    # Get the existing user from email if the OAuth provider gives us an email
    user = User.where(:email => auth.info.email).first if auth.info.email

    # Create the user if it is a new registration
    if user.nil?
      user = User.new(
        name: auth.extra.raw_info.name,
        #username: auth.info.nickname || auth.uid,
        email: auth.info.email.blank? ? TEMP_EMAIL : auth.info.email,
        password: Devise.friendly_token[0,20]
      )
      user.skip_confirmation!
      user.save!
    end

    identity.update_user!(user)

    user
  end

  def send_neighbor_notifications
    # We might potentially send notifications up to 40 miles away
    # Check each to see if they are within their preferred notification distance
    nearby_users(100).each do |neighbor|
      next if neighbor.disable_notifications?
      next if distance_to(neighbor) >= neighbor.notification_distance 
      deliver_neighbor_notification(neighbor)
    end
  end

  def set_location_timestamp(current_site)
    reload
    return false if location_timestamp.present? || lat.nil? || lng.nil?
    update_columns(location_timestamp: Time.now)
    set_map_occupant(current_site) #NOTE Method on 
    send_neighbor_notifications
    true
  end

  def is_visible?(template)
    case template
    when 'findgoplayers.com'
      location_timestamp.present?
    else
      false
    end
  end

  def after_database_authentication
    email_hash = Digest::MD5.hexdigest(email.strip.downcase)
    url = "http://en.gravatar.com/#{email_hash}.json"
    uri = URI.parse(url)

    http = Net::HTTP.new(URI.parse(url)) # get_response takes an URI object
    http.read_timeout = 20
    response = Net::HTTP.get_response(URI.parse(url)) # get_response takes an URI object

    begin
      rhash = JSON.parse(response.body)
    rescue JSON::ParserError
      return
    end
    
    begin
      thumbnail = rhash.fetch('entry').first.fetch('thumbnailUrl')
      update_attribute(:avatar, thumbnail)
    rescue
      return
    end

  end


  private

  def set_map_occupant(current_site)
    case current_site
    when 'findgoplayers.com'
      update_columns(is_go_player: true)
    end
  end

  def map_position_validation
    if lat.nil? || lng.nil?
      errors.add(:location, "Your approximate location must be marked on the map")
    end
  end

end
