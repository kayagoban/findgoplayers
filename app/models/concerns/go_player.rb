module GoPlayer
  extend ActiveSupport::Concern

  included do
    scope :visible_go_players, -> { User.visible.where(is_go_player: true) }
  end

  # for scopes, relations and other deep changes to the referring object, use 
  # include do
  # end

  #NOTE (kayagoban) Here we build a bunch of tuples to represent ranks.
  #                 Pros are '9p' through '1p', values are -16 to -8
  #                 Dans are '7d' through '1d', values are -7 to -1
  #                 Kyus are barely worth mentioning
  PRO_RANKS = Array(1..9).reverse.map{ |rank| ["#{rank}p", -rank -7] }
  DAN_RANKS = Array(1..7).reverse.map{ |rank| ["#{rank}d", -rank] }
  KYU_RANKS = Array(1..30).map { |rank| ["#{rank}k", rank] }

  RANKS = PRO_RANKS + DAN_RANKS + KYU_RANKS
  SELECTABLE_RANKS = DAN_RANKS + KYU_RANKS

  def player? 
    user_type == 0
  end

  def club?
    user_type == 1
  end

  def rank_string
    GoPlayer::RANKS.select { |rank_tuple| rank == rank_tuple.last }.flatten.first
  end

  def name_and_rank
    "#{nickname} (#{rank_string})"
  end

  def display_name
    name_and_rank
  end

  def deliver_neighbor_notification(neighbor)
    GoMailer.neighbor_notification(neighbor, self, distance_to(neighbor) ).deliver
  end

end
