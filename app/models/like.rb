class Like < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  validates_uniqueness_of :user, :scope => [:conversation]

  after_create :increment_count
  before_destroy :decrement_count


  def increment_count
    conversation.likes_count += 1
    conversation.save
  end

  def decrement_count
    conversation.likes_count -= 1
    conversation.save
  end
end
