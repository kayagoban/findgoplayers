class Conversation < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  has_many :commenters, through: :comments, source: :user
  has_many :likes
  has_many :likers, through: :likes, source: :user

  scope :listenable_for, 
    ->(listener) { joins(:comments).where('comments.user_id' => User.listenable_for(listener))}

  paginates_per 6

  # For pseudo-paginating of comments
  DEFAULT_COMMENTS = 4

  def origin
    comments.first
  end

  def like?(user)
    likers.include? user 
  end

end
