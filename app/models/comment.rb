class Comment < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  before_create :increment_count
  #before_destroy :decrement_count

  # We want comments_count on users unique to the conversation, to properly
  # gauge popularity
  def increment_count
    return if conversation.commenters.include? user 
    conversation.comments_count += 1
    conversation.save
  end

  # Ideally we would decrement only when we delete the last comment for a unique user
  # on this conversation. 
  # Just because somebody deletes their comment, does not make the conversation less
  # popular.  We will not decrement.
  def decrement_count
#    conversation.comments_count -= 1
#  conversation.save
  end

end
