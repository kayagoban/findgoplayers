class MessageMailer < ActionMailer::Base
  default from: "noreply@findgoplayers.com"

  def user_message(user, current_user, message)
    @user = user
    @message = message
    @sender = current_user
    @block_url = block_user_url(@sender.id)
    mail(to: @user.email, subject: "A message from #{current_user.nickname}(#{current_user.rank_string} on FindGoPlayers.com" )
  end

  def neighbor_notification(user, new_neighbor, message)
    set_neighbor_vars(new_neighbor)
    @user = user
    @account_url = account_url
    mail(to: @user.email, subject: "A new local Go Player near you")
  end

  private

  def set_neighbor_vars(new_neighbor)
    @new_neighbor = new_neighbor

    if new_neighbor.user_type == 0
      @user_type  =  'player'
      @rank_string = new_neighbor.rank_string
    else
      @user_type = 'club'
      @rank_string = ''
    end
  end

end
