class UsersController < ApplicationController
  # This controller is completed by a domain template.
  before_filter :obtain_user, only: [:post_location_update, :new_message, :post_message, :block, :show]
  before_filter :authenticate_user!, only: [:new_message, :post_message, :message, :block]
  respond_to :json, :html
  before_filter :extend_template_controller
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

  #NOTE (kayagoban) our included module cannot override our methods, so we'll call a proxy instead

  def index
    respond_to do |format|
      format.html  do
        expires_now() 
      end 
      format.json do
        @visible = visible_for_site
       render :partial => "templates/#{@template}/index.json.jbuilder"
      end
    end
  end

  def show
    @display_name = display_name
    respond_to do |format|
      #NOTE (kayagoban) render without layout because HTML is destined for gmaps infowindow
      format.html do
        @conv_count = Conversation.where(id: Conversation.listenable_for(@user).distinct).count
        render layout: false 
      end
      format.json do
        respond_with @user
      end
    end
  end

  def post_message
    quota_accounting

    if over_message_quota?
      return message_failure(
        "Sorry, you have exceeded your daily quota of #{User::MAX_MESSAGES_PER_DAY} messages.  Try again in about #{Time.now.seconds_until_end_of_day / 3600} hours."
      )
    end
    if @user.blockees.include? current_user
      return message_failure("You are not permitted to send messages to #{@user.name_and_rank}")
    end

    respond_to do |format|
      format.html do
        @message = params[:message] 
        send_user_message
        info_message = "You successfully sent your message to #{@user.nickname} (#{@user.rank_string}).  You have used #{@msg_count} out of your daily quota of #{User::MAX_MESSAGES_PER_DAY} messages."

        flash.keep[:success] = info_message
        return redirect_to root_url
      end

    end
  end

  def block
    if !current_user.blockees.include? @user
      current_user.blockees << @user
    end
    #TODO (kayagoban) use flash message instead
    flash.keep[:success] = info_message
    redirect_to root_url
  end



  # ONE_SHOT
  def update_location
  end

  def post_location_update
    @user.update_attributes(locality: params[:locality], admin_area_1: params[:admin_area_1], country: params[:country], loc_string: params[:loc_string])
  end
  # -- ONE_SHOT

  private

  # ONE_SHOT
  def location_update_params
    
  end
  # -- ONE_SHOT

  def visible_for_site
    case @template
    when 'findgoplayers.com'
      User.visible_go_players
    end
  end

  def extend_template_controller
    require Rails.root + 'templates' + @template + 'controllers' + 'users_controller.rb'
    case @template
    when 'findgoplayers.com'
      #  binding.pry
      extend Templates::FindGoPlayers::UsersController
    end
  end

  def message_failure(error_message)
    flash.now[:error] = error_message
    render 'new_message'
  end

  def over_message_quota?
    (current_user.msg_count + 1) > User::MAX_MESSAGES_PER_DAY
  end

  def quota_accounting
    @msg_count = if (current_user.last_message_at < Date.today)
                   1
                 else
                   return if over_message_quota?
                   current_user.msg_count + 1
                 end

    current_user.update_attributes(msg_count: @msg_count, last_message_at: Time.now)
  end

  def obtain_user
    @user = User.find user_params[:id]
  end

  def user_params
    return params.permit([:id, :format])
  end

end
