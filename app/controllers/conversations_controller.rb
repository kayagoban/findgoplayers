class ConversationsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json, :html
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

  def create
    new_conversation = Conversation.create!
    params[:conversation_id] = new_conversation.id
    params[:user_id] = current_user.id

    # test for null comment
    if params['text'].strip.empty?
      return render status: 501
    end

    Comment.create!(comment_params)

    @conversations = [ new_conversation ]
    @participants = [ current_user ] 

    respond_to do |format|
      format.json { 
        render partial: "shared/conversations_loop", layout: false, formats: [:html] 
      }
    end
  end

  def user_feed
    @target_user = User.find params.permit(:user_id)[:user_id]
    @unpaged_conversations = unpaged_for_user
    set_conversation_vars
    render partial: "shared/conversations_loop", layout: false, formats: [:html] 
  end

  # Feed for a user
  def home
    @target_user = current_user
    @unpaged_conversations = unpaged_for_user
    set_conversation_vars
    render partial: "shared/conversations", layout: false, formats: [:html] 
  end

  def top
    # ranking(id integer, comments integer, likes integer, popularity_weight integer)
    @unpaged_conversations = Conversation.all.order('ranking(id, comments_count, likes_count, 3) DESC').includes([:comments, :comments => :user], [:likers])
    set_conversation_vars
    render partial: "shared/conversations_loop", layout: false, formats: [:html] 
  end


  def comment
    params[:user_id] = current_user.id
    new_comment = Comment.create!(comment_params)

    # test for null comment
    if params['text'].strip.empty?
      return render status: 501
    end
 
    respond_to do |format|
      format.json { 
        render partial: "shared/comment", 
        layout: false, 
        formats: [:html], 
        :locals => { comment: new_comment }  
      }
    end
  end

  def comments
    @comments = Comment.where(conversation: comment_params[:conversation_id]).order(created_at: :asc).offset(Conversation::DEFAULT_COMMENTS)
    render partial: 'shared/paged_comments'
  end

  def like_toggle
    @conversation = Conversation.find(params.permit(:conversation_id)[:conversation_id])
    respond_to do |format|
      format.json do
        begin
          like = Like.create!(user: current_user, conversation_id: @conversation.id) 
        rescue
          like = Like.where(user: current_user, conversation_id: @conversation.id) 
          like.destroy_all
        end
        @conversation.reload
      end
    end
  end

    private

  def set_conversation_vars
    @more_pages = total_pages > requested_page
    @conversations = @unpaged_conversations.page(requested_page)
    @next_page = requested_page + 1
    @participants = User.joins(:comments).where('comments.conversation_id' => @conversations).distinct
  end

  def unpaged_for_user
    Conversation.where(id: Conversation.listenable_for(@target_user).distinct).includes([:comments, :comments => :user], [:likers]).order(created_at: :desc)
  end

  def total_pages
    @unpaged_conversations.page(1).total_pages
  end

  def requested_page 
    (params.permit(:page)[:page] || 1).to_i
  end

  def comment_params
    params.permit(:user_id, :text, :url, :url_title, :url_description, :url_thumbnail, :url_video, :conversation_id)
  end

end
