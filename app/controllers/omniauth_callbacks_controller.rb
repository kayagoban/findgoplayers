class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  before_filter :get_user

  def facebook
    @user.update_attribute(:avatar, env['omniauth.auth']['info']['image']);
    return oauth_redirect('Facebook') if @user.persisted? 
    session["devise.facebook_data"] = env["omniauth.auth"]
  end

=begin
"omniauth.auth"=>
  {"provider"=>"facebook",
   "uid"=>"100007875512408",
   "info"=>
    {"nickname"=>"cath.thomas.756",
     "email"=>"goishi.san@gmail.com",
     "name"=>"Cath Thomas",
     "first_name"=>"Cath",
     "last_name"=>"Thomas",
     "image"=>"http://graph.facebook.com/100007875512408/picture",
     "urls"=>{"Facebook"=>"https://www.facebook.com/cath.thomas.756"},
     "location"=>"Fayetteville, Arkansas",
     "verified"=>true},
=end

  #def linked_in
  #  return if is_persisted('LinkedIn')
  #  session["devise.linkedin_data"] = env["omniauth.auth"]
  #  redirect_to new_user_registration_url
  #end

  def google_oauth2
    @user.update_attribute(:avatar, env['omniauth.auth']['info']['image']);
    return oauth_redirect('Google') if @user.persisted?
    session["devise.google_data"] = env["omniauth.auth"]
  end

  private

  def get_user
    begin
      @user = User.find_for_oauth(env["omniauth.auth"], current_user)
    rescue ActiveRecord::RecordInvalid => e
      flash[:error] = e.to_s
      redirect_to new_user_session_path 
    end
  end

  def oauth_redirect(service)
    set_flash_message(:notice, :success, :kind => service) if is_navigational_format?
    sign_in_and_redirect @user, :event => :authentication
  end

end
