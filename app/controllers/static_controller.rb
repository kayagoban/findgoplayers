class StaticController < ApplicationController

  def hire_me
    expires_now()
  end
  
  def how_it_works
    expires_now()
  end

end
