class RegistrationsController < Devise::RegistrationsController
  before_filter :extend_template_controller
  before_filter :set_ranks

  def edit
    super
  end

  def update
    super
    if current_user.set_location_timestamp(@template)
      flash[:notice] = share_message
    end
  end

  private

  def share_message
     "You're all set up.  Tell your friends!  #{facebook_button} #{google_button} #{twitter_button}"
  end

  def google_button
    '<div data-annotation="none" class="g-plus" data-action="share" data-href="http://findgoplayers.com/"></div>'
  end

  def twitter_button
    '<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://findgoplayers.com/" data-count="none">Tweet</a>'
  end

  def facebook_button
    '<div class="fb-share-button" data-href="http://findgoplayers.com/" data-type="button">
    </div>'
  end


  def extend_template_controller
    require Rails.root + 'templates' + @template + 'controllers' + 'registrations_controller.rb'
    case @template
    when 'findgoplayers.com'
      extend Templates::FindGoPlayers::RegistrationsController
    end
  end

  #def after_inactive_sign_up_path_for(resource)
  #  binding.pry
  #  '/account'
  #end

  def after_sign_up_path_for(resource)
    account_path
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end



end
