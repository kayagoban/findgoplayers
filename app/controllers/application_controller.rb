class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :set_template
  before_filter :configure_devise_params, if: :devise_controller?
  before_filter :set_environment_variables

  protect_from_forgery with: :exception



  def set_environment_variables
    @gmaps_api_key ||= ENV['GMAPS_API_KEY']
  end

  private 

  def set_template 
    @template = request.domain
    case @template
    when 'findgoplayers.com'
      @title = "Find Go Players in your area"
    end
  end

  def configure_devise_params
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(paramalot)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(paramalot)
    end
  end

  def paramalot
    [:email, :password, :password_confirmation, :current_password, :nickname, :rank, :lat, :lng, :loc_string, :locality, :admin_area_1, :country, :blurb, :url, :user_type, :avatar, :notification_distance, :disable_notifications]
  end

  def after_sign_in_path_for(resource_or_scope)
    return account_path unless current_user.is_visible?(@template)
    stored_location_for(resource_or_scope) || root_path
  end

end
