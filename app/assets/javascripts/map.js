var map;
var i;
var mapData;
var markerClusterer = null;
var markers = [];
var userMarkersArray = [];
var infowindow = new google.maps.InfoWindow({
  content: "loading..." 
});


function initialize() {

  $.getJSON('users.json', function(data) {
    mapData = data;
    makeUserMap();
  });
}

$(document).ready(initialize);
//$(document).on('page:load', initialize);

function makeUserMap() {
  loadMap();

  /*
     $.blockUI({ message:'<div id="blockUIMessage">Downloading Map Data...</div>', css: { 
     border: 'none', 
     padding: '15px', 
     backgroundColor: '#000', 
     '-webkit-border-radius': '10px', 
     '-moz-border-radius': '10px', 
     opacity: .5, 
     color: '#fff' 
     } });  */

  //TODO show progress bar here

  for (var i = 0; i < mapData.length; i++) {

    var user = mapData[i];

    userMarker = placeMarker(latLngFromUser(user), user);

    userMarkersArray[user.id] = userMarker;

    markers.push(userMarker);
    //TODO update progress
  }

  //  $.unblockUI();

  var mcOptions = {gridSize: 35, maxZoom: 12};
  markerClusterer = new MarkerClusterer(map, markers, mcOptions);
  //TODO remove progress bar
}

function latLngFromUser(user) {
  Lat = parseFloat( user.lat );
  Lng = parseFloat( user.lng );
  return new google.maps.LatLng(Lat, Lng);
}

function loadMap() {
  var myLatlng   = new google.maps.LatLng(36,14);
  var mapOptions = {
    zoom: 2,
    center: myLatlng
  }
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
}

function placeMarker(location, user) {

   var marker = new google.maps.Marker({
      position: location,
        map: map,
        icon: mapIcon(user),
     animation: google.maps.Animation.DROP
   });
  
  google.maps.event.addListener(marker, 'click', function() { 
    fetchUserInfo(user.id, marker);
  });

  return marker
}


function fetchUserInfo(user_id, marker) {
  if (infowindow) {
    infowindow.close();
  }
  ga('send', 'pageview', "/users/" + user_id);

  $.get("users/" + user_id + ".html", function(userHtml) {
    infowindow = new google.maps.InfoWindow({
      content: userHtml
    });
    infowindow.open(map,marker);
    google.maps.event.addListener(infowindow,'domready',function(){
      openInfoWindowCallback(user_id);
    });
  });

}

function openInfoWindowCallback(user_id) {
  $("button#" + user_id + ".load-conv").bind('click', function() {
    show_user_tab(user_id);
  });
}


