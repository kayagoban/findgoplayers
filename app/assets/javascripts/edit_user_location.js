
var map;
var geocoder;
var userMarker = null;

function initialize() {
  loadMap();

  if (latLngPreloadsExist()) {
    userMarker = placeMarker(preloadLatLng());
  }

  // Update User Position
  google.maps.event.addListener(map, 'click', function(event) {
    if (userMarker != null) {
      userMarker.setMap(null);
    }
    userMarker = placeMarker(event.latLng);
    gcode(event.latLng);
  });
}

function gcode(latlng) {
  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
       loc_array = extract_location_string(results[1].address_components);
       setHiddenFields(latlng, loc_array);
      } else {
        alert("That doesn't seem to be a valid location.");
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}

function extract_location_string(result) {
  var info={};
  for(var i=0;i<result.length;++i) {
    if(result[i].types[0]=="locality"){
      info['locality'] = result[i].long_name
    }
    if(result[i].types[0]=="administrative_area_level_1"){
      info['admin_area_1'] = result[i].long_name
    }
    if(result[i].types[0]=="country"){ 
      if(result[i].long_name == 'undefined') {
        info['country'] = info['admin_area_1'];
        info['admin_area_1'] = null;
      }
      else {
        info['country'] = result[i].long_name;
      }
    }
  }
  return info;
}

function loc_string(loc_hash) {
  var str = "";

  if(loc_hash['locality'] != 'undefined' && loc_hash['locality'] != '' && loc_hash['locality'] != null) {
    str += (loc_hash['locality'] + ", ");
  }
  if(loc_hash['admin_area_1'] != 'undefined' && loc_hash['admin_area_1'] != '' && loc_hash['admin_area_1'] != null) {
    str += (loc_hash['admin_area_1'] + ", ");
  }
  if(loc_hash['country'] != 'undefined' && loc_hash['country'] != '' && loc_hash['country'] != null ) {
    str += loc_hash['country'];
  }
  return str;
}

function setHiddenFields(position, loc_array) {
  jQuery("#user_lat").val(position.lat());
  jQuery("#user_lng").val(position.lng());
  jQuery("#user_locality").val(loc_array['locality']);
  jQuery("#user_admin_area_1").val(loc_array['admin_area_1']);
  jQuery("#user_country").val(loc_array['country']);
  jQuery("#user_loc_string").val( loc_string(loc_array));
}

$(document).ready(initialize);

function loadMap() {
  var myLatlng = new google.maps.LatLng(36,70);
  var mapOptions = {
    zoom: userZoom(),
    center: myLatlng
  }
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  geocoder = new google.maps.Geocoder();
}

function userZoom() {
  if (has_latlng) {
    return 14
  }
  else {
    return 2
  }
}

function preloadLatLng() {
  preloadLat = parseFloat( latPreload() );
  preloadLng = parseFloat( lngPreload() );
  return new google.maps.LatLng(preloadLat, preloadLng);
}

function latPreload() {
  return $('#user_lat').val();
}

function lngPreload() {
  return $('#user_lng').val();
}

function latLngPreloadsExist() {
  return $('#user_lat').val() != "" && $('#user_lng').val() != "";
} 

function placeMarker(location) {
  var marker = new google.maps.Marker({
    position: location,
      map: map
  });
  map.setCenter(location);
  return marker;
}
