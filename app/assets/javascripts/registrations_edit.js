
$('#user_disable_notifications').change(
    function() {
      if ($(this).is(':checked')) {
        disable_notifications();
      }
      else {
        enable_notifications();
      }
    });

$('#user_user_type_0').click(function(){
  enable_player();
});

$('#user_user_type_1').click(function(){
  enable_club();
});


// Support methods

function enable_notifications() {
  jQuery('#notification_distance_group').show('slow')
}
function disable_notifications() {
  jQuery('#notification_distance_group').hide('slow')
}

function enable_player() {
  show_rank();
  hide_url();
}

function enable_club() {
  hide_rank();
  show_url();
}

function hide_rank() {
  jQuery('#user_rank').hide('slow')
}

function show_rank() {
  jQuery('#user_rank').show('slow')
}

function hide_url() {
  jQuery('#club_url').hide('slow')
}

function show_url() {
  jQuery('#club_url').show('slow')
}
