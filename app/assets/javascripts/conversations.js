// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var curImages = new Array();

// Use this to submit post
var urlData = {};

/* Takes form: 
 {
  "title"=>"Welcome to American Go Association | American Go Association",
    "description"=>"Wednesday May 28, 2014",
    "image_src"=>"http://usgo.org/files/aga_visa_card.png?random=1401320917106",
    "title" = "American Go Associations"
    "text"=>"hey what about http://usgo.org heah",
    "url"=>"http://usgo.org"
 }
*/

function show_nav_tab(tab_name) {
  home_tab = $('ul.conversations-nav li#' + tab_name + ' a');
  conversation_container = $('div.conversations-container');

  home_tab.tab('show');

  $.get("/conversations/" + tab_name, function(userHtml) {
    // delete conv container to reset jscroll
    conversation_container.remove();
    $('div.conversations-section').append("<div class='conversations-container'></div>");
    conversation_container = $('div.conversations-container');
    conversation_container.append(userHtml).hide().fadeIn();
    conversation_container.jscroll({debug: true, nextSelector: 'a.jscroll-next:last', padding: 111});
    });
}

function show_user_tab(user_id) {
  conversation_container = $('div.conversations-container');
  $('ul.conversations-nav li#user').show().find('a').tab('show');

  $.get("/users/" + user_id + "/conversations.html", function(userHtml) {
    // delete conv container to reset jscroll
    conversation_container.remove();
    $('div.conversations-section').append("<div class='conversations-container'></div>");
    conversation_container = $('div.conversations-container');
    conversation_container.append(userHtml).hide().fadeIn();
    conversation_container.jscroll({debug: true, nextSelector: 'a.jscroll-next:last', 
      padding: 111 });

    $('ul.conversations-nav li#user a').unbind('click').click(function() {
      show_user_tab(user_id);
    });
  });

  //binding for switching user feed
  $.getJSON("users/" + user_id + ".json", function(userData) {
    $('ul.conversations-nav li#user a span').text(" " + userData["display_name"]);
    tooltipText = 'Conversations local to ' + userData["display_name"];
    $('ul.conversations-nav li#user a').attr('data-original-title', tooltipText);
  });
  conversation_container.scrollTop(0);

}

function like_toggle(conversation_id) {
  likes_div = $('div#' + conversation_id + '.likes');
  star = likes_div.find('span.star-icon')
    ani_container = likes_div.find('.ani-container');
  animation = likes_div.find('span.like-animation');

  star.tooltip('hide');

  $.get("/conversations/" + conversation_id + "/like_toggle.json", function(data) {
    if (data["liked"] == true) {
      star.addClass("like");
      animation.text("+1");
    }
    else {
      star.removeClass("like");
      animation.text("-1");
    }
  next_ani = animation.clone(true);
  animation.show();
  animation.addClass("animated fadeOutUp").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    animation.remove();
  });
  ani_container.append(next_ani);
  likes_div.find('span.likes-count').text(data['likes_count']);
  star.attr('data-original-title', data['tooltip_text']);
  });
}

function loadComments(conversation_id) {
  $.get("/conversations/" + conversation_id + "/comments.html", function(commentsHtml) {
    $('li#' + conversation_id + '.load-comments').remove();
    $('ul#' + conversation_id + '.conversation-comments').append(commentsHtml).slideDown('slow');
  });
}

function switchUserFeed(user_id) {
  conversations = $('div.conversation')
    container = $('div.conversations-container')
    conversations.fadeOut(700, function() {
      $.get("/feed.html?user_id=" + user_id + "&page=1", function(userHtml) {
        //conversations.remove();
        container.empty();
        container.append(userHtml).hide().fadeIn();
        container.jscroll({nextSelector: 'a.jscroll-next:last', 
          padding: 5});
        conversationPage = 1;
      });

    });
}

function closePreview() {
  var liveUrl = $('#create.liveurl');
  liveUrl.hide('fast');
  liveUrl.find('.video').html('').hide();
  liveUrl.find('.image').html('');
  liveUrl.find('.controls .prev').addClass('inactive');
  liveUrl.find('.controls .next').addClass('inactive');
  liveUrl.find('.thumbnail').hide();
  liveUrl.find('.image').hide();

  $('textarea#new_conversation').trigger('clear'); 
  curImages = new Array();
};

function submitComment(conversationId, userId) {
  // Set button inactive and clear all textaarea and preview elements
  inputField = $('textarea#' + conversationId + '.new_comment');
  inputData = { "text": inputField.val() };
  inputField.val("");

  $.post('/conversations/' + conversationId + '/comment.json', inputData, 
      function(data) {
        // Set button active again
        // reload conversations
        //append data to the conversations container

        commentId = $(data).attr("id");
        $('ul#' + conversationId + '.conversation-comments').append(data);
        newComment = $('li#' + commentId + '.comment');
        newComment.hide();
        participantSelector = $("#" + userId + ".user-link.comment-" + commentId);
        participantSelector.bind('click', function() { alert('test') });
        participantSelector.hover(function() {
          $(this).css('cursor','pointer');
        });

        newComment.slideDown(800);
      },
      'html'
      );

  closePreview();
}


function submitConversation() {
  // Set button inactive and clear all textaarea and preview elements
  textarea = $('textarea#new_conversation')
    urlData["text"] = textarea.val();
  textarea.val("");

  $.post('/conversations.json', urlData, 
      function(data) {
        // Set button active again
        // reload conversations
    urlData = {};
  //append data to the conversations container
  $(data).insertAfter( "div#new.conversation" );
  $("div.conversation:eq(1)").hide();
  $("div.conversation:eq(1)").fadeIn(800);
      },
      'html'
      );

  closePreview();
}

function attachLiveURL() {
  $('textarea#new_conversation').liveUrl({
    loadStart : function(){
      $('.liveurl-loader').show();
    },
  loadEnd : function(){
    $('.liveurl-loader').hide();
  },
  success : function(data) {                        
    var output = $('div#create.liveurl');
    output.find('.title').text(data.title);
    output.find('.description').text(data.description);
    output.find('.url').text(data.url);
    output.find('.image').empty();

    output.find('.close').one('click', closePreview);

    output.show('fast');

    if (data.video != null) {                       
      var ratioW        = data.video.width  /350;
      data.video.width  = 350;
      data.video.height = data.video.height / ratioW;

      var video = 
        '<object width="' + data.video.width  + '" height="' + data.video.height  + '">' +
        '<param name="movie"' +
        'value="' + data.video.file  + '"></param>' +
        '<param name="allowScriptAccess" value="always"></param>' +
        '<embed src="' + data.video.file  + '"' +
        'type="application/x-shockwave-flash"' +
        'allowscriptaccess="always"' +
        'width="' + data.video.width  + '" height="' + data.video.height  + '"></embed>' +
        '</object>';
      output.find('.video').html(video).show();


    }

    urlData["url"] = data.url;
    urlData["url_title"] = data.title;
    urlData["url_description"] = data.description;

  },
  addImage : function(image)
  {   

    var output  = $('#create.liveurl');
    var jqImage = $(image);
    jqImage.attr('alt', 'Preview');

    if ((image.width / image.height)  > 7 
        ||  (image.height / image.width)  > 4 ) {
          // we dont want extra large images...
          return false;
        } 

    curImages.push(jqImage.attr('src'));
    output.find('.image').append(jqImage);

    if (curImages.length == 1) {
      // first image...

      output.find('.thumbnail .current').text('1');
      output.find('.thumbnail').show();
      output.find('.image').show();
      jqImage.addClass('active');

      urlData["url_thumbnail"] = jqImage.attr('src');
    }

    if (curImages.length == 2) {
      output.find('.controls .next').removeClass('inactive');
    }

    output.find('.thumbnail .max').text(curImages.length);

  }
  });


}


$('.liveurl ').on('click', '.controls .button', function() 
    {
      var self        = $(this);
      var liveUrl     = $(this).parents('.liveurl');
      var content     = liveUrl.find('.image');
      var images      = $('img', content);
      var activeImage = $('img.active', content);

      if (self.hasClass('next')) 
  var elem = activeImage.next("img");
      else var elem = activeImage.prev("img");

      if (elem.length > 0) {
        activeImage.removeClass('active');
        elem.addClass('active');  
        // urlData["url_thumbnail"] = elem.src;
        urlData["url_thumbnail"] = elem.attr('src');

        liveUrl.find('.thumbnail .current').text(elem.index() +1);

        if (elem.index() +1 == images.length || elem.index()+1 == 1) {
          self.addClass('inactive');
        }
      }

      if (self.hasClass('next')) 
        var other = elem.prev("img");
      else var other = elem.next("img");

      if (other.length > 0) {
        if (self.hasClass('next')) 
          self.prev().removeClass('inactive');
        else   self.next().removeClass('inactive');
      } else {
        if (self.hasClass('next')) 
          self.prev().addClass('inactive');
        else   self.next().addClass('inactive');
      }

    });

