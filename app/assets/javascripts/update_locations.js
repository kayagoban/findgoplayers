

var geocoder = new google.maps.Geocoder();



$.getJSON('/users.json', function(userData) {


  var count = userData.length;

  console.log('Processing ' + count + ' users');
  for(var i = 0; i < count; i++) {
    setTimeout(processUser(userData, i), ((i+1) * 60000));
  }

});

function processUser(userData, i) {
  return function() {
      console.log('Processing user ' + (i+1) + ' with lat:' + userData[i]['lat'] + ', lng:' + userData[i]['lng']  );
      latlng =  new google.maps.LatLng(userData[i]['lat'], userData[i]['lng'] );

      gcode(latlng, userData[i]['id']);
  }

}

function gcode(latlng, i) {
  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        loc_array = extract_location_string(results[1].address_components);
        putInfo(latlng, loc_array, i);
      } else {
        //alert("That doesn't seem to be a valid location.");
      }
    } else {
      console.log('************** Geocoder failed due to: ' + status);
    }
  });
}

function extract_location_string(result) {
  var info={};
  for(var i=0;i<result.length;++i) {
    if(result[i].types[0]=="locality"){
      info['locality'] = result[i].long_name
    }
    if(result[i].types[0]=="administrative_area_level_1"){
      info['admin_area_1'] = result[i].long_name
    }
    if(result[i].types[0]=="country"){ 
      if(result[i].long_name == 'undefined') {
        info['country'] = info['admin_area_1'];
        info['admin_area_1'] = null;
      }
      else {
        info['country'] = result[i].long_name;
      }
    }
  }
  return info;
}

function putInfo(position, loc_array, i) {
  var update_obj = { 
    id: i,
    lat: position.lat(),
    lng: position.lng(),
    locality: loc_array['locality'],
    admin_area_1: loc_array['admin_area_1'],
    country: loc_array['country'],
    loc_string: loc_string(loc_array)
  }

  console.log('Updating user ' + update_obj['id'] +  ' ---  lat:' + update_obj['lat'] + ' , lng:' + update_obj['lng'] + ', locality:' + update_obj['locality'] + ', admin_area_1:' + update_obj['admin_area_1'] +
      ', country:' + update_obj['country'] + ', loc_string:' + update_obj['loc_string']);

  $.post('/users/' + i + '/post_location_update.json', update_obj, function(response) {
    // Do something with the request
  }, 'json');
}

function loc_string(loc_hash) {
  var str = "";

  if(loc_hash['locality'] != 'undefined' && loc_hash['locality'] != '' && loc_hash['locality'] != null) {
    str += (loc_hash['locality'] + ", ");
  }
  if(loc_hash['admin_area_1'] != 'undefined' && loc_hash['admin_area_1'] != '' && loc_hash['admin_area_1'] != null) {
    str += (loc_hash['admin_area_1'] + ", ");
  }
  if(loc_hash['country'] != 'undefined' && loc_hash['country'] != '' && loc_hash['country'] != null ) {
    str += loc_hash['country'];
  }
  return str;
}
