json.array! @visible do |user|
  json.id user.id
  json.lat user.lat
  json.lng user.lng
  json.rank user.rank
  json.rank_string user.rank_string
  json.user_type user.user_type
end
