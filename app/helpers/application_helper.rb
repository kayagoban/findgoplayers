module ApplicationHelper

  def bootstrap_class_for flash_type
    case flash_type
    when 'success'
      "alert-success" # Green
    when 'error'
      "alert-danger" # Red
    when 'alert'
      "alert-warning" # Yellow
    when 'notice'
      "alert-info" # Blue
    else
      flash_type.to_s
    end
  end

  def avatar_url(user)
    if user.avatar.present?
      user.avatar
    else
      "#{root_url}default_avatar.png"
   end
  end

  #       gravatar_id = Digest::MD5::hexdigest(user.email).downcase
    #  "http://gravatar.com/avatar/#{gravatar_id}.png?s=48&d=#{CGI.escape(default_url)}"
 

end
