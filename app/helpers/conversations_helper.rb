module ConversationsHelper

  def time_difference(target_time)
    diff = Time.zone.now - target_time
    if diff < 2.minutes
      "just now"
    elsif diff < 2.hours
      "#{(diff / 1.minutes).floor} minutes ago"
    elsif diff < 2.day
      "#{(diff / 1.hours).floor} hours ago"
    elsif diff < 2.months
      "#{(diff / 1.day).floor} days ago"
    elsif diff < 2.years
      "#{(diff / 1.month).floor} months ago"
    else
      "#{(diff / 1.years).floor} years ago"
    end
  end

  def like_class(conversation)
    return "like" if conversation.like?(current_user)
    ""
  end

  def like_toggle_text(conversation)
    conversation.like?(current_user) ? "Click to unrecommend story" : "Click to recommend story"
  end

end
