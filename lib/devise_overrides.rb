module Devise
  module Models
    module DatabaseAuthenticatable

      def update_without_password(params, *options)

        if params[:password].blank?
          params.delete(:password)
          params.delete(:password_confirmation) if params[:password_confirmation].blank?
        end

        result = update_attributes(params, *options)
        clean_up_passwords
        result
      end

    end
  end
end



